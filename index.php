<?php
/* Index page of odmetools.
 *
 * Copyright (C) 2015  Ralf Kruedewagen <gpl@kruedewagen.de>.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<!-- <meta charset="iso-8859-1">--><!--  Das RIS verwendet kein UTF-8, load() Ergebnis umwandeln -->
	<title>Webtools für Offene Daten Mettmann</title>
	
	<!-- <link rel="stylesheet" href="lib/css/ui-lightness/jquery-ui-1.10.3.custom.min.css"> -->
	<!-- <link rel="stylesheet" href="lib/DataTables/media/css/offene-daten-me-webtools_table.css">-->
	<!-- <link rel="stylesheet" href="lib/DataTables/extras/TableTools/media/css/TableTools.css">-->
		
	<script src="lib/js/jquery-1.11.2.min.js"></script>
	<script src="include/js/offene-daten-me-webtools.js"></script>
	<!-- <script src="lib/DataTables/media/js/jquery.dataTables.min.js"></script>-->
    <!-- <script src="lib/DataTables/extras/TableTools/media/js/TableTools.js"></script>-->
    <!-- <script src="lib/DataTables/extras/TableTools/media/js/ZeroClipboard.js"></script>-->
	
</head>
<body>
<h2>Webtools für Offene Daten Mettmann</h2>

<?php
require("include/php/lib.inc.php");
?>

<?php
	//var_dump($_REQUEST);
	switch ($_POST['action'])
	{
		case "ris2meta":
			ris2meta();
			break;
		default:
			showForm();
			break;
			
	}
?>

<br />

<em>Powered by <a target="_blank" href="http://mettmann.piratenpartei-nrw.de">Piraten in Mettmann</a>.
Technische Realisierung dieses Tools: Ralf Krüdewagen. Source Code unter GPL v3 bei <a target="_blank" href="https://github.com/formwandler/odmetools">GitHub</a>. Version <?php echo $version;?> vom <?php echo $date;?>.</em>

</body>
</html>

<?php
/* Part of odmetools.
 * Loads an external page as plain source code, can be inserted in DOM via jQuery .load() method.
 *
 * Copyright (C) 2015  Ralf Kruedewagen <gpl@kruedewagen.de>.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require("include/php/lib.inc.php");

$convert_to_utf8 = true; // if page is in ISO-8859-1 

if(isset($_GET['url']))
{
	$url =  htmlspecialchars($_GET['url']);

	if(validateUrl($url,$risServer))
	{
		if ($convert_to_utf8)
			echo utf8_encode(file_get_contents($url));
		else
			echo file_get_contents($url);
	}
	else
	{
		echo "Fehler: URL enthält unerwünschte Zeichen oder ist ungültig.";
		exit (1);
	}
}

?>
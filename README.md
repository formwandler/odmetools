odmetools
=========

Tools for [Offene Daten Mettmann](http://www.offene-daten-mettmann.de).

## Purpose
This tool fetches data from the [Ratsinformationssystem](https://mettmann.more-rubin1.de) (RIS) of Mettmann City (Stadt Mettmann) in order to manually import datasets and resources to the CKAN instance of [Offene Daten Mettmann](http://www.offene-daten-mettmann.de).

It's is highly adapted to the RIS delivered by "more rubin", see [more! rubin](http://www.more-rubin.de).

## Installation
* Just copy the PHP and JavaScript files to your webserver document root directory

## Requirements
* [jQuery](http://jquery.com) (tested with version 1.11.2)

## TODO
* Feature: Make it configurable in order to adapt it easier to DOM changes in the RIS.
* Feature: Make static parameters (like license output) configurable.
* Feature: Import via CKAN API instead of manual import.

## Copying and License

This material is Copyright (C) 2015  Ralf Kruedewagen <gpl@kruedewagen.de>.

It is open source and licensed under the GNU GPL v3.0 whose full text may be found at http://www.gnu.org/licenses/gpl-3.0.html. See also LICENSE file.

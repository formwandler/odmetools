/* Part of odmetools.
 * JavaScript functions.
 *
 * Copyright (C) 2015  Ralf Kruedewagen <gpl@kruedewagen.de>.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


		function getMetaAndPrintDataset(contentURI) {
			
			// ---------------------------------
			// Datum, to use e.g. in title
			// ---------------------------------
			//var dateTitle = $("div#content>div.InfoBlock:first>table.tabelleninhalt>tbody>tr:first>td.Ungerade_Zeile:first").text(); // class InfoBlock
			//var ckanDate = $("div#content div.InfoBlock:first table.tabelleninhalt tbody tr td.Ungerade_Zeile:eq(1)").text(); // class InfoBlock
			//var ckanDsDate = $("div#content>div.InfoBlock:first>table.tabelleninhalt>tbody>tr:first>td.Ungerade_Zeile:eq(1)").text(); // Mettmann only
			var ckanDsDate = $("div#content div.InfoBlock:first>table>tbody>tr:first>td:eq(1)").text(); // more general
			
			// e.g. "05.02.2015, 17:00 Uhr"
			var myarr1 = ckanDsDate.split(".");
			var yearTime = myarr1[2]; // "2015, 17:00 Uhr"
			var myarr2 = yearTime.split(",");
			var year = myarr2[0];
			
			// ---------------------------------
			// Titel
			// ---------------------------------
			// remove (öffentlich/nichtöffentlich) and append year to title
			var ckanDsTitleFull = $("div#content b.Suchueberschrift:first").text(); // class Suchueberschrift
			var titleArr = ckanDsTitleFull.split("(");
			var ckanDsTitle = titleArr[0]+" "+year;
			
			$("<tr><td>Titel</td><td>"+ckanDsTitle+"</td><td>Jahr aus dem Datum im RIS extrahiert.</td></tr>").appendTo("table#dataset");
			
			// ---------------------------------
			// Beschreibung
			// ---------------------------------
			// Starting with e.g. "2. Sitzung" will render as numbered list -> set it as strong
			// add (öffentlich/nichtöffentlich) here
			var ckanDsDescription = "**"+ckanDsTitle+" ("+titleArr[1]+"**. Quelle: [Ratsinformationssystem der Stadt Mettmann] ("+contentURI+" \"Ratsinformationssystem der Stadt Mettmann\")";
			$("<tr><td>Beschreibung</td><td>"+ckanDsDescription+"</td><td>CKAN Markdown Formatierung</td></tr>").appendTo("table#dataset");

			// ---------------------------------
			// Schlagworte (Tags)
			// ---------------------------------
			var ckanDsTags = "";
			$("<tr><td>Schlagworte (Tags)</td><td>"+ckanDsTags+"</td><td>noch manuell hinzufügen</td></tr>").appendTo("table#dataset");

			// ---------------------------------
			// Lizenz
			// ---------------------------------
			var ckanDsLicense = "Creative Commons Attribution";
			$("<tr><td>Lizenz</td><td>"+ckanDsLicense+"</td><td>manuell hinzugefügt</td></tr>").appendTo("table#dataset");

			// ---------------------------------
			// Organisation
			// ---------------------------------
			var ckanDsOrganisation = "ak-mettmann";
			$("<tr><td>Organisation</td><td>"+ckanDsOrganisation+"</td><td>manuell hinzugefügt</td></tr>").appendTo("table#dataset");

			// ---------------------------------
			// Sichtbarkeit
			// ---------------------------------
			var ckanDsVisibility = "Öffentlich";
			$("<tr><td>Sichtbarkeit</td><td>"+ckanDsVisibility+"</td><td>manuell hinzugefügt</td></tr>").appendTo("table#dataset");

			// ---------------------------------
			// Some empty lines to split the 1st from 2nd part, since here we need to add a resource
			// ---------------------------------
			$("<tr><td></td><td></td><td></td></tr>").appendTo("table#dataset");
			$("<tr><td></td><td>Jetzt muss zunächst eine Ressource über \"Daten hinzufügen\" hinzugefügt werden. Die Felder unten werden dann später dem Datensatz hinzugefügt.</td><td></td></tr>").appendTo("table#dataset");
			$("<tr><td></td><td></td><td></td></tr>").appendTo("table#dataset");
			
			// ---------------------------------
			// Quelle
			// ---------------------------------
			$("<tr><td>Quelle</td><td>"+contentURI+"</td><td>URL zur Sitzung</td></tr>").appendTo("table#dataset");

			// ---------------------------------
			// Autor
			// ---------------------------------
			var ckanDsAuthor = "Stadt Mettmann";
			$("<tr><td>Autor</td><td>"+ckanDsAuthor+"</td><td>manuell hinzugefügt</td></tr>").appendTo("table#dataset");

			// ---------------------------------
			// Custom Field 1 - Datum ---- noch formatieren?!
			// ---------------------------------
			$("<tr><td>Key [Datum]</td><td>"+ckanDsDate+"</td><td>Custom Field 1 (Datum aus der RIS-Seite)</td></tr>").appendTo("table#dataset");

			// ---------------------------------
			// Gruppen (Kategorien)
			// ---------------------------------
			var ckanDsGroup = "Ratsinformationssystem";
			$("<tr><td>Gruppen (Kategorien)</td><td>"+ckanDsGroup+"</td><td>manuell hinzugefügt</td></tr>").appendTo("table#dataset");

		}

		function getMetaAndPrintResources(contentURI) {

			// get just the protocol and server part of the full URL to build download URL
			var serverURIarray = contentURI.split("/");
			var serverURI = serverURIarray[0]+"//"+serverURIarray[2]; // like http://www.domain.de
			
			// count InfoBlock elements, last infoBlock contains the resources (not always the 4th, e.g. if "Teilnehmer" block is empty it's the 3rd)
			var infoblockCount = $("div#content>div.InfoBlock").length;
			//$("<span>Anzahl der gefundenen InfoBlock elements: "+infoblockCount+"<br /></span>").appendTo("p.resourcecount");
			var resourceIndex = infoblockCount - 1; // last one contains resources, index starts with 0, could also use $("div#content>div.InfoBlock:first")

			// Count all table rows from resource InfoBlock, just for output
			var resourcecount = $("div#content>div.InfoBlock:eq("+resourceIndex+")>table>tbody>tr").length;
			$("<span>Anzahl der gefundenen Ressourcen: "+resourcecount+"</span>").appendTo("p.resourcecount");

			// ---------------------------------
			// Loop through resources from last InfoBlock
			// ---------------------------------
			$("div#content>div.InfoBlock:eq("+resourceIndex+")>table>tbody>tr").each(function( index ) {
				//console.log( index + ": " + $(this).text() );
				
				// Ressource No.
				var ckanRsResourceNo = index + 1;
				$("<tr><td><p></p></td></tr>").appendTo("table.resources"); // empty line
				$("<tr><td>Ressource Nummer: "+ckanRsResourceNo+"</td></tr>").appendTo("table.resources");
								
				// Add a new table per resource, take care lo use:last for adding content
				$('<tr><td><table class="resource" border="1" cellspacing="0" cellpadding="2"></table></td></tr>').appendTo("table.resources");
				$('<thead><tr><th>Feld</th><th>Wert</th><th>Hinweis</th></tr></thead>').appendTo("table.resource:last");

				// ---------------------------------
				// Ressource URL, form in 6th <td> element
				// ---------------------------------
				// Show only if the form exists 
				// result must be something like https://mettmann.more-rubin1.de/show_pdf.php?_typ_432=vorl&_doc_n1=20132404100183.pdf&_nk_nr=2013&_nid_nr=20132404100183&_neu_dok=&status=1
				var textTemplate = "";
				var foundTemplate = false;
				
				// form building the link from Template (Vorlage)
				// by default the "Vorlage" is the resource URL
				// put also to "Beschreibung"
				if ($(this).children().eq(5).children().attr("action")) {
					
					var ckanRsResource = serverURI+"/"+$(this).children().eq(5).children().attr("action")+"?";
					//+"?"+$(this).children().eq(5).find("input").attr("name");
					
					var thisInputs = $(this).children().eq(5).find("input"); // all input elements
					var nameCount = 0;
					foundTemplate = true;
					
					// Loop through form inputs
					$(thisInputs).each(function( inputIndex ) {

						// add all existing name/value pairs to URL
						if ($(this).attr("name")) {

							// add "&" before each parameter pair (but not the first)
							if (nameCount > 0) {
								ckanRsResource =  ckanRsResource + "&";
							}

							ckanRsResource = ckanRsResource + $(this).attr("name") + "=" + $(this).attr("value");
							nameCount++;
						}
					});
					
					textTemplate = '.<br /><br />[Vorlage] (<a target="_blank" href="'+ckanRsResource+'">'+ckanRsResource+'</a> "Vorlage")';
				}
				else {
					var ckanRsResource = "";
				}				
				$('<tr><td>Ressource</td><td><a target="_blank" href="'+ckanRsResource+'">'+ckanRsResource+'</a></td><td></td></tr>').appendTo("table.resource:last");

				// ---------------------------------
				// Name, 5th <td> element
				// ---------------------------------
				var ckanRsName = $(this).children().eq(4).text();
				var ckanRsShortName = "";
				
				// remove leading "[]" if existing
				if (ckanRsName.match("]")) {
					var nameArray = ckanRsName.split("]");
					ckanRsShortName = nameArray[1];
				}
				else {
					ckanRsShortName = ckanRsName;
				}
				
				$("<tr><td>Name</td><td>"+ckanRsShortName+"</td><td></td></tr>").appendTo("table.resource:last");

				// ---------------------------------
				// Beschreibung
				// ---------------------------------
				// repeat name and add attachement (Anlagen) link from 7th <td> element and Beschlüsse link from 8th <td> element
				var ckanRsDescription = "";
				var textAttachment = "";
				var textResolution = "";
				var ckanRsDescriptionNotice = "CKAN Markdown Formatierung";
				
				// even if ckanRsResource is empty, there can be a "Beschluss" (Resolution)
				ckanRsDescription = ckanRsName;
				
				// form building the "Anlagen" link
				var urlAttachments = "";
				var foundAttachment = false;
				var foundResolution = false;
				if ($(this).children().eq(6).children().attr("action")) {

					var urlAttachments = serverURI+"/"+$(this).children().eq(6).children().attr("action")+"?";
					
					var thisInputs = $(this).children().eq(6).find("input"); // all input elements
					var nameCount = 0;
					foundAttachment = true;
					
					// Loop through form inputs
					$(thisInputs).each(function( inputIndex ) {

						// add all existing name/value pairs to URL
						if ($(this).attr("name")) {

							// add "&" before each parameter pair (but not the first)
							if (nameCount > 0) {
								urlAttachments =  urlAttachments + "&";
							}

							urlAttachments = urlAttachments + $(this).attr("name") + "=" + $(this).attr("value");
							nameCount++;
						}
					});
					textAttachment = '.<br /><br />[Link zu Anlagen] (<a target="_blank" href="'+urlAttachments+'">'+urlAttachments+'</a> "Link zu Anlagen")';
				}
				
				// form building the "Beschluss" link
				var urlResolution = "";
				if ($(this).children().eq(7).children().attr("action")) {

					var urlResolution = serverURI+"/"+$(this).children().eq(7).children().attr("action")+"?";
					
					var thisInputs = $(this).children().eq(7).find("input"); // all input elements
					var nameCount = 0;
					foundResolution = true;
					
					// Loop through form inputs
					$(thisInputs).each(function( inputIndex ) {

						// add all existing name/value pairs to URL
						if ($(this).attr("name")) {

							// add "&" before each parameter pair (but not the first)
							if (nameCount > 0) {
								urlResolution =  urlResolution + "&";
							}

							urlResolution = urlResolution + $(this).attr("name") + "=" + $(this).attr("value");
							nameCount++;
						}
					});
					textResolution = '.<br /><br />[Beschluss] (<a target="_blank" href="'+urlResolution+'">'+urlResolution+'</a> "Beschluss")';
				}
				
				$('<tr><td>Beschreibung</td><td>'+ckanRsDescription+textTemplate+textAttachment+textResolution+'</td><td>'+ckanRsDescriptionNotice+'</td></tr>').appendTo("table.resource:last");

				// ---------------------------------								
				// Format
				// ---------------------------------
				var ckanRsFormat = "";
				// maybe seach for PDF in title or action script instead of ==
				if ($(this).children().eq(5).children().attr("action") == "show_pdf.php" || $(this).children().eq(7).children().attr("action") == "show_pdf.php") {
					ckanRsFormat = "PDF";
				}
				$("<tr><td>Format</td><td>"+ckanRsFormat+"</td><td></td></tr>").appendTo("table.resource:last");
				
				// hide or fade out table if no resource found
				if ( !foundTemplate && !foundAttachment && !foundResolution) {
					//$("table.resource:last").hide();
					$("table.resource:last").fadeOut( 10000 );
				}
				
				
			}); // end of resource loop from resource InfoBlock
			
			// ---------------------------------
			// Other Dokuments: Protokoll, Einladung
			// ---------------------------------
			
			// Count all table rows, just for output
			var docCount = $("div#content>div:eq(4)>table>tbody>tr>td>div.InfoBlock:eq(0)>div").length;
			if (docCount > 0) {
				$("<span><br />Anzahl der gefundenen weiteren Dokumente: "+docCount+"</span>").appendTo("p.resourcecount");
			}
			docNo = 0;
			
			$("div#content>div:eq(4)>table>tbody>tr>td>div.InfoBlock:eq(0)>div").each (function( index ) {
				
				// Ressource No.
				docNo = index + 1;
				$("<tr><td><p></p></td></tr>").appendTo("table.resources"); // empty line
				$("<tr><td>Weiteres Dokument Nummer: "+docNo+"</td></tr>").appendTo("table.resources");
				
				// Add a new table per resource, take care lo use:last for adding content
				$('<tr><td><table class="resource" border="1" cellspacing="0" cellpadding="2"></table></td></tr>').appendTo("table.resources");
				$('<thead><tr><th>Feld</th><th>Wert</th><th>Hinweis</th></tr></thead>').appendTo("table.resource:last");
				
				// ---------------------------------
				// Resource
				// ---------------------------------
				var urlDoc = "";
				var textDoc = "";
				var foundDoc = false;
				

				if ($(this).find("form").attr("action")) {

					urlDoc = serverURI+"/"+$(this).find("form").attr("action")+"?";
					
					var thisInputs = $(this).find("form").find("input"); // all input elements
					var nameCount = 0;
					foundDoc = true;
					
					// Loop through form inputs
					$(thisInputs).each(function( inputIndex ) {

						// add all existing name/value pairs to URL
						if ($(this).attr("name")) {

							// add "&" before each parameter pair (but not the first)
							if (nameCount > 0) {
								urlDoc =  urlDoc + "&";
							}

							urlDoc = urlDoc + $(this).attr("name") + "=" + $(this).attr("value");
							nameCount++;
						}
					});
					textDoc = '.<br /><br />[Dokument] (<a target="_blank" href="'+textDoc+'">'+textDoc+'</a> "Dokument")';
				}
				
				$('<tr><td>Ressource</td><td><a target="_blank" href="'+urlDoc+'">'+urlDoc+'</a></td><td></td></tr>').appendTo("table.resource:last");
				
				// ---------------------------------
				// Name
				// ---------------------------------
				var ckanRsName = $(this).text();
				$("<tr><td>Name</td><td>"+ckanRsName+"</td><td></td></tr>").appendTo("table.resource:last");

				// ---------------------------------
				// Beschreibung
				// ---------------------------------
				ckanRsDescription = ckanRsName;
				var ckanRsDescriptionNotice = "CKAN Markdown Formatierung";
				$('<tr><td>Beschreibung</td><td>'+ckanRsDescription+'</td><td>'+ckanRsDescriptionNotice+'</td></tr>').appendTo("table.resource:last");

				// ---------------------------------								
				// Format
				// ---------------------------------
				var ckanRsFormat = "";
				// maybe seach for PDF in title or action script instead of ==
				if ($(this).find("form").attr("action") == "show_pdf.php") {
					ckanRsFormat = "PDF";
				}
				$("<tr><td>Format</td><td>"+ckanRsFormat+"</td><td></td></tr>").appendTo("table.resource:last");
				
			});

		} // end of function

<?php
/* Part of odmetools.
 * PHP functions.
 *
 * Copyright (C) 2015  Ralf Kruedewagen <gpl@kruedewagen.de>.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

$version = "0.2.4";
$date = "25.01.2014";
$risServer = "more-rubin1.de"; // external URL to be validated against, without http://


/*
 * @param String $server (server to be checked)
 * @return Boolean
 */
function validateUrl($url,$server = "mettmann.more-rubin1.de")
{
	if (isset($server) && $server != "")
		//$pattern = "/^(http|https):\/\/(".$server."\/"."):?(\d+)?\/?/i"; // exact match the server
		$pattern = "/^(http|https):\/\/([A-Z0-9_-]+[\.]{1})*(".$server."\/"."):?(\d+)?\/?/i"; // host can differ
	else
		$pattern = "/^(http|https):\/\/([A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i"; // any valid URL
	
	//preg_match( '/^(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}'.'((:[0-9]{1,5})?\/.*)?$/i' ,$uri)
	
	return (bool)preg_match($pattern, $url);
}

function showForm()
{
?>

<p>Bitte die URL zum zu untersuchenden RIS-Eintrag eingeben,
z.B. <a target="_blank" href="https://mettmann.more-rubin1.de/sitzungen_top.php?sid=2013-PL-23">so einen Link</a>
oder einen <a target="_blank" href="https://mettmann.more-rubin1.de/sitzungen_top.php?sid=ni_2013-HF-12">älteren</a> mit Beschlüssen
aus dem <a target="_blank" href="https://mettmann.more-rubin1.de">Ratsinformationssystem</a> der Stadt Mettmann. Der Link muss den "sid=" Parameter enthalten.
</p>

<form action="index.php" method="post">
	<fieldset>
		<legend>RIS to Meta</legend>
		<label for="url">URL</label><br />
		<input name="url" id="url" type="text" size="100" /><br />
		<input type="hidden" name="action" value="ris2meta" />
		<input type="submit" name="submit" value="Go" /><br />
	</fieldset>
</form>

<p>Hinweis:
Der "sid" Parameter muss hier unbedingt angegeben werden. Man erhält ihn a) entweder direkt aus den Links der zukünftigen Ratssitzungen
auf der RIS-Startseite. Oder b) aus dem Quelltext, wenn man im RIS die Recherche oder den Kalender nutzt.
Im Fall von b) kann man dazu in Firefox mit rechter Maustaste "Element untersuchen" auswählen oder die Erweiterung "Firebug" verwenden.
</p>
<?php
}

function ris2meta()
{
	global $risServer;
	$url =  htmlspecialchars($_POST['url']);
	
	// Go on if the page is validated to avoid loading any page into the DOM (which will be executed!!)
	if(validateUrl($url,$risServer))
	{
		?>
		<p class="result">Meta-Daten für die URL <a href="<?php echo $url;?>"><?php echo $url;?></a>. </p>
		
		<p class="status" id="status">Bitte warten. Daten werden verarbeitet.</p>
		
		<p>Hinweise für das Eintragen in CKAN:</p>
		<ul>
			<li>Die Werte können per Copy/Paste in die jeweiligen CKAN-Felder übernommen werden.</li>
			<li>Die URL eines CKAN-Datensatzes kann bei der Eingabe nicht automatisch erstellt werden, wenn der Name des Datensatzes mit einer Ziffer beginnt. Nach Fehlermeldung muss man eine URL-Pfad manuell eingeben.</li>
			<li>Die letzten vier Paremeter des Datensatzes werden erst abgefragt, wenn man die erste Resource angegeben hat.</li>
			<li>Dem Datensatz sollte mindestens eine Gruppe (Kategorie) im entsprechenden Reiter zugewiesen werden. Es können später weitere Gruppen (Kategorien) hinzugefügt werden.</li>
			<li>Die Ressourcen werden über "Link" hinzugefügt.</li>
			<li>Wenn zu einzelnen Ressourcen Anlagen existieren, wird derzeit nur auf die betreffende Seite im Ratsinformationsystem verlinkt.
			Dieser Link wird in die Beschreibung eingefügt.</li>
			<li>Die Ressource URL wird auf die Vorlage (PDF) gesetzt, wenn eine Vorlage vorhanden ist.</li>
			
		</ul>
		
		<p>ToDo</p>
		<ul>
			<li>Datum in ein ISO-Format bringen, z.B. "YYYY-MM-DD HH:MM:SS"</li>
			<li>Wie erstellt man eine URL zur Eingabe in dieses Tool aus der Recherche-Funktion des RIS?</li>
			<li>Wie zwischen Vorlage und Beschluss unterscheiden? Tags, Gruppen (Kategorien), eigene Ressourcen in CKAN?</li>
			<li>Tag "öffentlich", "nichtöffentlich" setzen - aus dem Titel abgeleitet.</li>
		</ul>
				
		<h2>Datensatz (Dataset)</h2>
		<table id="dataset" class="display" border="1" cellspacing="0" cellpadding="2">
				<thead>
					<tr>
						<th>Feld</th>
						<th>Wert</th>
						<th>Hinweis</th>
					</tr>
				</thead>
		</table>
		
		<h2>Ressourcen</h2>
		<p class="resourcecount" id="resourcecount"></p>
		<table id="resources" class="resources" border="0" cellspacing="0" cellpadding="2">
				
		</table>
		
		<?php
		loadExternalPage($url);
		//loadIntoIframe($url);
		
	}
	else
	{
		echo "Fehler: URL enthält unerwünschte Zeichen oder ist ungültig.";
		exit (1);
	}
}

function loadExternalPage($url)
{
	?>
	<div id="externalPage"></div>	
	
	<script type='text/javascript'>

		// style status message displayed during page load
		$("#status").css({
		"color":"red",
		"background":"yellow"
			});
	
		$(function(){
    		var contentURI= '<?php echo $url;?>';
    		var myElement = "#top"; // set #id or .class for the external element to be loaded

    		// just load an id or class to avoid loading javascript in page head etc.
    		// wait for load to be finished, so run the meta processing and output from the callback function
    		$('#externalPage').load('loadext.php?url='+contentURI+' '+myElement,null,function() {
        		
    			$("#status").hide(); // hide the status text from being displayed after external page has loaded

    			getMetaAndPrintDataset(contentURI);
    			getMetaAndPrintResources(contentURI);

    			// Ausgabe zur Info
    			var lastActual =  $("div.aktualisierung").text();
    			$("<p>"+lastActual+" (das RIS betreffend)</p>").appendTo("p.result");

    			//$('#dataset').dataTable();
    			//$('#resources').dataTable();
    		});
    		
		});
		
		$("#externalPage").hide(); // hide the page from being displayed
	</script>
	
	<?php
}

// DOM in iFrame not accessible via jQuery, only iframe object as a whole
function loadIntoIframe($url)
{
	echo "<p>Die Seite wird in einem iFrame dargestellt.</p>";
	/* $('<div><iframe src="<?php echo $url;?>" width="300" height="300" /></div>').dialog({ modal: true, width: 300, height: 300}); */
	
	/*
	 * $('Hallo.').appendTo("body");
	 */
	
	?>
		<script type='text/javascript'>
			$('<iframe src="<?php echo $url;?>" width="300" height="300" />').appendTo("body");
		</script>
	
	<?php
}

// DOM in iFrame not accessible via jQuery, only iframe object as a whole
function loadIntoIframe2($url)
{
	echo "<p>Die Seite wird in einem iFrame dargestellt.</p>";
	?>
	<iframe src="" id="urlIframe">
        <p>Your browser does not support iframes.</p>
    </iframe>
    <script type='text/javascript'>
    	$("#urlIframe").attr('src', <?php echo $url;?>);
    </script>
	<?php
}




?>